#include <Arduino.h>
#include <TimeLib.h>
#include <TimeAlarms.h>
#include <states.h>
#include <pin_management.h>

int current_state;
AlarmId id;

// return enum value of current state
int getCurrentState()
{
    return current_state;
}

// set the current state value
void setCurrentState(int state)
{
    current_state = state;
}

// method that turns off lights when timer is triggered
void TurnOffLightsAfterTimer()
{
    Serial.println("Timer triggered. Turning off lights");
    // call method to turn off lights
    turnLightsOff();
}

// set the 5min timer for lights
// if previous timer is set update to 5min
void setTimerForLights()
{
    if (Alarm.isAllocated(id))
    {
        // has to be this way, because Alarm.write caused triggering the event
        Alarm.free(id);
        id = Alarm.timerOnce(5*60, TurnOffLightsAfterTimer); // auto turn off lights after 5min
    }
    else
    {
        id = Alarm.timerOnce(5*60, TurnOffLightsAfterTimer); // auto turn off lights after 5min
    }
}

// control the state of doors
void updateState()
{
    if (isDoorOpen())
    {
        if (getCurrentState() == DOOR_OPEN)
        {
            return;
        }

        setCurrentState(DOOR_OPEN);
        Serial.println("Door open. Lights on timer.");
        turnLightsOn();
        setTimerForLights();

        return;
    }

    if (isDoorClosed())
    {
        if (getCurrentState() == DOOR_CLOSED)
        {
            return;
        }

        setCurrentState(DOOR_CLOSED);
        Serial.println("Door closed. Lights on timer.");
        turnLightsOn();
        setTimerForLights();

        return;
    }

    if (isDoorMoving())
    {
        // update state from closed or open
        // otherwise keep state (opening, closing, invalid)
        if (getCurrentState() == DOOR_OPEN)
        {
            setCurrentState(DOOR_CLOSING);
            Serial.println("Door closing");
        }
        else if (getCurrentState() == DOOR_CLOSED)
        {
            setCurrentState(DOOR_OPENING);
            Serial.println("Door opening");
        }

        return;
    }

    // sensor detected invalid state
    setCurrentState(INVALID_STATE);
    Serial.println("Invalid state");
}

// return human readable message for current state
String getStateMessage()
{
    switch (current_state)
    {
    case DOOR_OPENING:
        return "Door is opening ...";
    case DOOR_CLOSING:
        return "Door is closing ...";
    case DOOR_CLOSED:
        return "Door is closed";
    case DOOR_OPEN:
        return "Door is open";
    default:
        return "Invalid state";
    }
}