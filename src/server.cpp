#include <server.h>
#include <ESPAsyncWebServer.h>
#include <states.h>
#include <pin_management.h>
#include <OTA.h>
#include <Update.h>

AsyncWebServer server(80);

void notFoundResponse(AsyncWebServerRequest *request)
{
    request->send(404, "text/plain", "Not found");
}

String prepareStatusResponseMessage()
{
    DynamicJsonDocument message(1024);
    String serializedMessage;

    message["state"] = getCurrentState();
    message["message"] = getStateMessage();

    serializeJson(message, serializedMessage);
    return serializedMessage;
}

// handle the upload of the firmware
void handleUpload(AsyncWebServerRequest *request, String filename, size_t index, uint8_t *data, size_t len, bool final)
{
    // handle upload and update
    if (!index)
    {
        Serial.printf("Update: %s\n", filename.c_str());
        if (!Update.begin(UPDATE_SIZE_UNKNOWN))
        { //start with max available size
            Update.printError(Serial);
        }
    }

    /* flashing firmware to ESP*/
    if (len)
    {
        Update.write(data, len);
    }

    if (final)
    {
        if (Update.end(true))
        { //true to set the size to the current progress
            Serial.printf("Update Success: %ub written\nRebooting...\n", index+len);
        }
        else
        {
            Update.printError(Serial);
        }
    }
    // alternative approach
    // https://github.com/me-no-dev/ESPAsyncWebServer/issues/542#issuecomment-508489206
}

// mains erver handling block
void startServer()
{
    // REST API
    server.on("/ping", HTTP_GET, [](AsyncWebServerRequest *request) {
        AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", "OK");
        request->send(response);
    });
    server.on("/door/activate", HTTP_POST, [](AsyncWebServerRequest *request) {
        triggerDoor();
        delay(1000);
        AsyncWebServerResponse *response = request->beginResponse(200, "application/json", prepareStatusResponseMessage());
        request->send(response);
    });
    server.on("/light/switch", HTTP_POST, [](AsyncWebServerRequest *request) {
        String message;
        if (lightsOn())
        {
            turnLightsOff();
            message = "Lights are Off";
        }
        else
        {
            turnLightsOn();
            message = "Lights are On";
        }

        AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", message);
        request->send(response);
    });
    server.on("/door/status", HTTP_GET, [](AsyncWebServerRequest *request) {
        AsyncWebServerResponse *response = request->beginResponse(200, "application/json", prepareStatusResponseMessage());
        request->send(response);
    });
    server.onNotFound(notFoundResponse);

    // web interface for OTA
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
        AsyncWebServerResponse *response = request->beginResponse(200, "text/html", indexPage);
        response->addHeader("Connection", "close");
        request->send(response);
    });
    server.on("/firmware", HTTP_GET, [](AsyncWebServerRequest *request) {
        AsyncWebServerResponse *response = request->beginResponse(200, "text/html", firmwarePage);
        response->addHeader("Connection", "close");
        request->send(response);
    });
    // handling uploading firmware file
    server.on("/update", HTTP_POST, [](AsyncWebServerRequest *request) {
        if (!Update.hasError()) {
            AsyncWebServerResponse *response = request->beginResponse(200, "text/plain", "OK");
            response->addHeader("Connection", "close");
            request->send(response);
            ESP.restart();
        } else {
            AsyncWebServerResponse *response = request->beginResponse(500, "text/plain", "ERROR");
            response->addHeader("Connection", "close");
            request->send(response);
        } }, handleUpload);
    server.begin();
    Serial.println("HTTP Server running");
}