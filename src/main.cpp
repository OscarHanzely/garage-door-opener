#include <Arduino.h>
#include <wifi_management.h>
#include <pin_management.h>
#include <states.h>
#include <server.h>
#include <TimeAlarms.h>

void setup()
{
  // initialize LED digital pin as an output.
  pinMode(LED_BUILTIN, OUTPUT);

  // initialize Serial output
  Serial.begin(9600);
  // initialize pins
  initPins();
  // connect to wifi
  initWiFi();
  // start webserver
  startServer();
}

void writeLED()
{
  switch (getCurrentState())
  {
  case DOOR_OPENING:
  case DOOR_CLOSING:
    // blink the LED
    digitalWrite(LED_BUILTIN, digitalRead(LED_BUILTIN) ^ 1);
    break;
  case DOOR_CLOSED:
    digitalWrite(LED_BUILTIN, LOW);
    break;
  case DOOR_OPEN:
    digitalWrite(LED_BUILTIN, HIGH);
    break;
  default:
    // ERROR state => blink twice
    digitalWrite(LED_BUILTIN, LOW);
    delay(200);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(200);
    digitalWrite(LED_BUILTIN, LOW);
    delay(200);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(200);
    digitalWrite(LED_BUILTIN, LOW);
    break;
  }
}

void loop()
{
  // check/reconnect wifi
  keepAliveWiFi();

  // update the current door state
  updateState();

  // set the LED
  writeLED();

  // wakeup Alarms - this code is a must otherwise that shit will never start servicing alarms
  Alarm.delay(0);

  // 2sec
  delay(2000);
}