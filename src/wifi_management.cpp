#include <Arduino.h>
#include <WiFi.h>
#include <config.h>
#include <TimeLib.h>
#include <WiFiUdp.h>
#include <Syslog.h>

bool wifiStatus = false;
WiFiUDP udpClient;

IPAddress syslogIp() 
{
    IPAddress syslog_ip = IPAddress();
    if ( !syslog_ip.fromString((char *)SYSLOG_SERVER_IP) ) {
        Serial.println("Failed to parse the IP of syslog server");
    }

    return syslog_ip;
}

// Create a new syslog instance with LOG_KERN facility
Syslog logger(udpClient, syslogIp(), (uint16_t)SYSLOG_PORT, (char *)DEVICE_HOSTNAME, (char *)APP_NAME, LOG_USER, SYSLOG_PROTO_BSD);

//--------------------------------------------------------------------------------------------------
// Function Details
//--------------------------------------------------------------------------------------------------
// Name : wifiClear
// Purpose : clears all wifi settings before connecting. must be run before every wifiConnect
// Argument(s) : void
// Returns : void

void wifiClear()
{
    WiFi.softAPdisconnect(true); // disconnects AP Mode
    WiFi.mode(WIFI_STA);
    WiFi.disconnect(); // disconnects STA
    delay(1000);
    wifiStatus = WiFi.status() == WL_CONNECTED;
}

//--------------------------------------------------------------------------------------------------
// Function Details
//--------------------------------------------------------------------------------------------------
// Name : wifiConnect
// Purpose : connects to given ssid using password for given number of tries. updates the wifiStatus.
// Argument(s) : char *SSID, char *PASS, unsigned short tries
// Returns : void

void wifiConnect(char *SSID, char *PASS, unsigned short tries)
{
    WiFi.persistent(false);
    wifiStatus = WiFi.status() == WL_CONNECTED;
    while ((tries > 0) && (!wifiStatus))
    {
        Serial.print(".");
        wifiClear();
        WiFi.begin(SSID, PASS);
        delay(5000);
        wifiStatus = WiFi.status() == WL_CONNECTED;
        tries--;
    }
}

//--------------------------------------------------------------------------------------------------
// Function Details
//--------------------------------------------------------------------------------------------------
// Name : wifiDisconnect
// Purpose : disconnects from the network and switches off the radio. sets wifiStatus to false.
// Argument(s) : void
// Returns : void

void wifiDisconnect()
{
    WiFi.disconnect();
    WiFi.mode(WIFI_OFF);
    wifiStatus = false;
}

// blink three times if connection failed
void WiFiErrorLED()
{
    for (int a = 0; a < 3; a++)
    {
        digitalWrite(LED_BUILTIN, LOW);
        delay(200);
        digitalWrite(LED_BUILTIN, HIGH);
        delay(200);
        digitalWrite(LED_BUILTIN, LOW);
    }
}

// setClock sync method for device time
void setClock()
{
    if (WiFi.isConnected())
    {
        // first parameter is adjustment in seconds (GMT-7h)
        configTime(-25200, 0, "north-america.pool.ntp.org");
        Serial.print(F("Waiting for NTP time sync: "));
        time_t nowSecs = time(nullptr);
        while (nowSecs < 8 * 3600 * 2)
        {
            delay(500);
            Serial.print(F("."));
            yield();
            nowSecs = time(nullptr);
        }
        Serial.println();
        struct tm timeinfo;
        gmtime_r(&nowSecs, &timeinfo);
        Serial.print(F("Current time: "));
        Serial.print(asctime(&timeinfo));
        setTime(timeinfo.tm_hour,timeinfo.tm_min,timeinfo.tm_sec,timeinfo.tm_mday,timeinfo.tm_mon,timeinfo.tm_year);
    }
    else
    {
        Serial.println("Can't sync clock. Wifi disconnected.");
    }
}

// initWifi method responsible for connecting to wifi network
void initWiFi()
{
    WiFi.setAutoConnect(true);
    WiFi.setAutoReconnect(true);
    Serial.print("Connecting to WiFi ");
    wifiConnect((char *)WIFI_SSID, (char *)WIFI_PASSWD, 5);
    if (!WiFi.isConnected())
    {
        Serial.println("Failed to connect to wifi.");
        WiFiErrorLED();
        return;
    }
    int rssi = WiFi.RSSI();
    IPAddress ip = WiFi.localIP();
    Serial.printf("\nIP address: %s, Signal strength: %d dBm, SSID: %s\n", ip.toString().c_str(), rssi, WiFi.SSID().c_str());
  
    logger.logf("WiFi Connected successfully SSID %s, IP: %s", WiFi.SSID().c_str(), ip.toString().c_str());

    setClock();
}

void keepAliveWiFi()
{
    if (!WiFi.isConnected())
    {
        Serial.println("Wifi disconnected. Reconnecting");
        initWiFi();
        setClock();
    }
    logger.logf("Connected to wifi, IP: %s, Signal strength: %d dBm", WiFi.localIP().toString().c_str(), WiFi.RSSI());
}
