#include <Arduino.h>

const int CLOSE_SWITCH_PIN = GPIO_NUM_14; // door closed magnet switch
const int OPEN_SWITCH_PIN = GPIO_NUM_15;  // door open magnet switch
const int DOOR_RELAY = GPIO_NUM_21; // relay for door operation
const int LIGHT_SWITCH_RELAY = GPIO_NUM_16; // relay for lights operation

void initPins();
bool lightsOn();
void turnLightsOff();
void turnLightsOn();
void triggerDoor();
bool isDoorOpen();
bool isDoorClosed();
bool isDoorMoving();

String pinValue(int pin_number);