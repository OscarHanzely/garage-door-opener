typedef enum
{
    INVALID_STATE = 0, /*!< state not detected */
    DOOR_CLOSED = 1,   /*!< door sensor detects closed state */
    DOOR_OPEN = 2,     /*!< door sensor detects open state  */
    DOOR_OPENING = 3,  /*!< door is opening  */
    DOOR_CLOSING = 4,  /*!< door is closing  */
} door_state;

void updateState();
int getCurrentState();
String getStateMessage();