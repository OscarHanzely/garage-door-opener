#include <pin_management.h>
#include <Arduino.h>

bool isDoorOpen()
{
  return digitalRead(OPEN_SWITCH_PIN) == LOW && digitalRead(CLOSE_SWITCH_PIN) == HIGH;
}

bool isDoorClosed()
{
  return digitalRead(CLOSE_SWITCH_PIN) == LOW && digitalRead(OPEN_SWITCH_PIN) == HIGH;
}

bool isDoorMoving()
{
  return digitalRead(OPEN_SWITCH_PIN) == HIGH && digitalRead(CLOSE_SWITCH_PIN) == HIGH;
}

// trigger method
void triggerDoor()
{
  digitalWrite(DOOR_RELAY, LOW); // relay on
  delay(1000);                    // 1sec
  digitalWrite(DOOR_RELAY, HIGH);  // relay off
}

// triggers lights relay
void turnLightsOn()
{
  digitalWrite(LIGHT_SWITCH_RELAY, LOW);
}

// triggers lights relay
void turnLightsOff()
{
  digitalWrite(LIGHT_SWITCH_RELAY, HIGH);
}

// status of garage ligts
bool lightsOn()
{
  return digitalRead(LIGHT_SWITCH_RELAY) == LOW;
}

// initialize controller pins
void initPins()
{
  pinMode(CLOSE_SWITCH_PIN, INPUT_PULLUP);
  pinMode(OPEN_SWITCH_PIN, INPUT_PULLUP);
  pinMode(DOOR_RELAY, OUTPUT);
  pinMode(LIGHT_SWITCH_RELAY, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(DOOR_RELAY, HIGH);
  digitalWrite(LIGHT_SWITCH_RELAY, HIGH);
  digitalWrite(LED_BUILTIN, LOW);
}

// internal method for testing
String pinValue(int pin_number)
{
  if (digitalRead(pin_number) == HIGH)
  {
    return "HIGH";
  }

  return "LOW";
}